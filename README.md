# TXA

## Installation

After cloning repository locally, `npm install` to install dependencies.

## Compiling

`npx hardhat compile`

## Testing

`npx hardhat test`

## License

See `LICENSE.md`

## Audit Report

A [security audit](certik-txa_token_security_audit-no_form_id-no_doc_id-2017-07-19.pdf) of the codebase by Certik is available as a PDF in the repository.
