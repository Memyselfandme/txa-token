/**
 *  Copyright (C) 2021 TXA Pte. Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  HardhatRuntimeEnvironment,
} from "hardhat/types";
import { DeployFunction } from 'hardhat-deploy/types';

const { ERC1820_REGISTRY_DEPLOY_TX, ERC1820_REGISTRY_ADDRESS } = require("@openzeppelin/test-helpers/src/data")

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, ethers, network } = hre;

  // Deploy ERC1820Registry
  if ((await ethers.provider.getCode(ERC1820_REGISTRY_ADDRESS)).length <= '0x0'.length) {
    let res = await (await ethers.getSigners())[0].sendTransaction({
      to: "0xa990077c3205cbDf861e17Fa532eeB069cE9fF96",
      value: ethers.utils.parseEther("0.08")
    })
    await res.wait()
    console.log("Sent 0.08 ETH to ERC1820 deployer address");
    let tx = await ethers.provider.sendTransaction(ERC1820_REGISTRY_DEPLOY_TX)
    await tx.wait()
    console.log("Deployed ERC1820");
  }
};

export default func;
